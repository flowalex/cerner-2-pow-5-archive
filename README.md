# Cerner's 2^5 Challenge Archive

Previous Cerner's 2^5 Programmers Day Challenge Submissions, each year will be in it's own folder

## Rules
1. 1 submission per day
2. 32 lines or less
3. Comments don't count as lines
